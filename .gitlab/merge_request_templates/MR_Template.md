[Mudanças propostas. O que foi feito no PR.]

[Em caso de bug :bug:, contextualizar o problema e referenciar a issue] Fixes #...
* ...
* ...

## Setup

[O que precisa ser feito para testar o PR]

## Testando 

### Cenário de teste

[Descrição geral do cenário]

1. Passo 1
2. Passo 2
3. Passo n

[Resultado esperado]

## Concerns e observações :thought_balloon:

[Pontos importantes e que merecem atenção]

## Ao deployar :ship:

[Qualquer processo ou algo importante na etapa de deploy]